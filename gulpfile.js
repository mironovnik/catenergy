"use strict";

var gulp = require("gulp");//подключение gulp
var server = require("browser-sync").create();//плагин для создания "живого" локального сервера
var sass = require("gulp-sass");//подключение пакета для компиляции sass в css
var plumber = require("gulp-plumber"); //непрерывание автоматизации при ошибке компиляции какого-либо файла, показывает ошибку
var postcss = require("gulp-postcss");// плагин для постобработки css
var posthtml = require("gulp-posthtml");// плагин для постобработки html
var autoprefixer = require("autoprefixer");// плагин для расстановки префиксов
var minify = require("gulp-csso"); //плагин, позволяющий минифицировать css файл
var rename = require("gulp-rename"); //плагин для переименования файлов
var imagemin = require("gulp-imagemin-fix"); //плагин для минификации изображений
var webp = require("gulp-webp");// плагин для конвертации изображений в webp
var svgstore = require("gulp-svgstore");// плагин для сборки svg спрайтов
var woff = require("gulp-ttf2woff");
var woff2 = require("gulp-ttf2woff2");
var del = require("del");
var uglify = require("gulp-uglify");
const babel = require('gulp-babel');
const svgo = require("gulp-svgo");


//таски для конвертации шрифта ttf в woff и woff2
function convertToWoff() {
    return gulp.src(["source/fonts/*.ttf"])
        .pipe(woff())
        .pipe(gulp.dest("source/fonts"));
}
function convertToWoff2() {
    return gulp.src(["source/fonts/*.ttf"])
        .pipe(woff2())
        .pipe(gulp.dest("source/fonts"));
}
gulp.task('convertWoff',gulp.parallel(convertToWoff,convertToWoff2));

function clean() {
    return del("build");
} //очистка папки build
function copy(){
    return gulp.src([
        "source/fonts/**/*.woff",
        "source/fonts/**/*.woff2",
        "source/img/**",
        "source/js/**",
        "source/*.html",
        "source/*.js"
    ], {
        base: "source"
    })
        .pipe(gulp.dest("build"));
} //копирование файлов в папку build
function style(){
    return gulp.src("source/sass/style.scss")
        .pipe(plumber())
        .pipe(sass())
        .pipe(postcss([
            autoprefixer()
        ]))
        .pipe(gulp.dest("build/css"))
        .pipe(minify())
        .pipe(rename("style.min.css"))
        .pipe(gulp.dest("build/css"))
        .pipe(server.stream());

} //компиляция scss в css, прогонка через autoprefixer, минификация
function compressJs(){
    return gulp.src("source/js/*.js")
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest("build/js"))
        .pipe(uglify())
        .pipe(rename("script.min.js"))
        .pipe(gulp.dest("build/js"))
        .pipe(server.stream());
}//прогонка через babel, минификация
function optimizeSvg() {
    return gulp.src("source/img/*.svg")
        .pipe(svgo())
        .pipe(gulp.dest("source/img"))
}
gulp.task('optimizeSvg', optimizeSvg);//таск оптимизации svg
function sprite(){
    return gulp.src("source/img/icon-*.svg")
        .pipe(svgstore({
            inlineSvg: true
        }))
        .pipe(rename("sprite.svg"))
        .pipe(gulp.dest("build/img"));
}//таск для сборки svg-спрайта
function html() {
    return gulp.src("source/*.html")
        .pipe(posthtml())
        .pipe(gulp.dest("build"))
        .pipe(server.stream());
}//обработка html
function images(){
    return gulp.src("source/img/**/*.{png, jpg, svg}")
        .pipe(imagemin([
            imagemin.optipng({optimizationLevel: 3}),
            imagemin.jpegtran({progressive: true}),
            imagemin.svgo()
        ]))
        .pipe(gulp.dest("build/img"));
}//минификация png, jpeg, svg
function toWebp() {
    return gulp.src("source/img/**/*.{png,jpg}")
        .pipe(webp({quality: 90}))
        .pipe(gulp.dest("build/img"));
}//перевод png, jpeg в webp
function serve(){
    server.init({
        server: "build/"
    });
    gulp.watch("source/sass/**/*.{scss, sass}", gulp.series(html,style));
    gulp.watch("source/*.html", gulp.series(html));
    gulp.watch("source/js/*.js", gulp.series(compressJs));
} //таск запуска живого сервера для отслеживания изменений css, html и js


var build = gulp.series(clean, gulp.parallel(convertToWoff,convertToWoff2, style, optimizeSvg, sprite, html, images, toWebp, compressJs), copy);
gulp.task("build", build);
var liveServer = gulp.series(serve);
gulp.task("serve", liveServer);